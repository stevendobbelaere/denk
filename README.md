# Denk website

### To run locally:

1) Clone this repository:  

2) Go into the newly created folder.  

3) Install node modules and bower components.  
`npm install && bower install`

4) Start the server.  
`gulp`

5) Go to localhost:8080
